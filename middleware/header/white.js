export default function ({ store }) {
  store.commit('theHeader/SET_DARK', false)
  store.commit('theHeader/SET_TRANSPARENT', false)
  store.commit('theHeader/SET_WHITE', true)
}

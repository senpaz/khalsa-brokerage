export default function ({ store }) {
  store.commit('theHeader/SET_DARK', false)
  store.commit('theHeader/SET_TRANSPARENT', true)
  store.commit('theHeader/SET_WHITE', false)
}

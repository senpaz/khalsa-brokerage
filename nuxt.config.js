export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TMS2',
    htmlAttrs: { lang: 'en' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/scss/main.scss'],
  styleResources: {
    scss: [
      '~/assets/scss/variables.scss',
      '~/assets/scss/mixins/index.scss',
      '~/assets/scss/vendors/smart-grid.scss'
    ]
  },
  loading: {
    color: '#2C6EEF',
    failedColor: '#EF2C2D',
    height: '3px',
    throttle: 0
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/routes.js' },
    { src: '~/plugins/api.js' },
    { src: '~/plugins/dayjs.js' },
    { src: '~/plugins/vue-loading-overlay.js' },
    { src: '~/plugins/vue-multiselect.js' },
    { src: '~/plugins/vue-svgicon.js' },
    { src: '~/plugins/axios.js' },
    { src: '~/plugins/vuelidate.js' },
    { src: '~/plugins/vue-awesome-swiper.js', mode: 'client' },
    { src: '~/plugins/vue-notifications.js', mode: 'client' },
    { src: '~/plugins/vuejs-paginate.js', mode: 'client' },
    { src: '~/plugins/vue-calendar.js', mode: 'client' },
    { src: '~/plugins/vue-tooltip.js', mode: 'client' },
    { src: '~/plugins/vue-modal.js', mode: 'client' },
    { src: '~/plugins/vue-scrolling-table.js', mode: 'client' },
    { src: '~/plugins/mixins.js' },
    { src: '~/plugins/vue-gmap.js', mode: 'client' }
  ],
  router: {
    prefetchLinks: false
    // linkExactActiveClass: 'menu-active-class'
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/proxy',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/sentry',
    'cookie-universal-nuxt',
    '@nuxtjs/style-resources',
    '@nuxtjs/i18n',
    '@nuxtjs/auth-next',
    'nuxt-stripe-module'
  ],
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.APP_BASE_URL,
    proxy: true,
    progress: false
  },
  proxy: {
    '/auth': {
      xfwd: true,
      target: process.env.APP_AUTH_URL
    },
    '/api': {
      xfwd: true,
      target: process.env.APP_AUTH_URL
    }
  },
  // sentry: {
  //   dsn: 'https://55ca3022baec4b2991c4598787c61a26@o1176625.ingest.sentry.io/6274589',
  //   // dsn: process.env.APP_SENTRY_DSN,
  //   // disabled: process.env.APP_SENTRY_DISABLED === 'true' || false,
  //   // publishRelease: process.env.SENTRY_PUBLISH_RELEASE === 'true' || false,
  //   tracing: {
  //     tracesSampleRate: 0.2,
  //     vueOptions: {
  //       tracing: true,
  //       tracingOptions: {
  //         hooks: ['mount', 'update'],
  //         timeout: 2000,
  //         trackComponents: true
  //       }
  //     },
  //     browserOptions: {}
  //   }
  // },
  auth: {
    plugins: [{ src: '~/plugins/axios', ssr: true }],
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/app'
    },
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'access_token',
          global: true,
          type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token',
          required: true,
          data: 'refresh_token',
          tokenRequired: true,
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'user',
          autoFetch: true
        },
        endpoints: {
          login: { url: '/auth/p/token', method: 'post' },
          refresh: { url: '/auth/r/token', method: 'post' },
          logout: { url: 'auth/logout', method: 'post' },
          user: { url: '/auth/user', method: 'get' }
        }
      }
    }
  },
  stripe: {
    publishableKey: process.env.APP_STRIPE_PUBLISHABLE_KEY
  },
  i18n: {
    langDir: '~/locales/',
    locales: [
      { code: 'en', iso: 'en-US', file: 'en.js', name: 'English' },
      { code: 'ru', iso: 'ru-RU', file: 'ru.js', name: 'Russian' }
    ],
    lazy: true,
    defaultLocale: 'en',
    // strategy: 'prefix_except_default',
    strategy: 'no_prefix',
    vueI18nLoader: true,
    detectBrowserLanguage: false,
    vueI18n: {
      silentTranslationWarn: true,
      silentFallbackWarn: true
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}

export default {
  sign_in: 'Sign in',
  profile: 'Profile',
  'Field is required.': 'Обязательно для заполнения',
  'Email is invalid.': 'Неверный формат email'
}

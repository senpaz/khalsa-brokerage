/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-news-more-arrow': {
    width: 74,
    height: 15,
    viewBox: '0 0 74 15',
    data: '<path pid="0" d="M67.343.43l6.364 6.363a1 1 0 010 1.414l-6.364 6.364a1 1 0 01-1.414-1.414L70.586 8.5H1a1 1 0 110-2h69.586l-4.657-4.657A1 1 0 0167.343.43z"/>'
  }
})

/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-checked': {
    width: 10,
    height: 8,
    viewBox: '0 0 10 8',
    data: '<path pid="0" d="M9.271.372c.251.203.304.59.118.864l-4.016 5.93c-.434.641-1.26.775-1.845.299l-2.8-2.279a.657.657 0 01-.116-.864.534.534 0 01.79-.128l2.8 2.279a.178.178 0 00.263-.043L8.481.5a.534.534 0 01.79-.128z"/>'
  }
})

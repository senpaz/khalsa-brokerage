/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'sign/icon-sign-back': {
    width: 10,
    height: 16,
    viewBox: '0 0 10 16',
    data: '<path pid="0" d="M9.254.241c.298.292.325.75.081 1.072l-.08.092L2.526 8l6.727 6.595c.298.292.325.75.081 1.072l-.08.092a.852.852 0 01-1.094.08l-.094-.08L.746 8.582A.811.811 0 01.665 7.51l.08-.092L8.068.241a.852.852 0 011.187 0z"/>'
  }
})

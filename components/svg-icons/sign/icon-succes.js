/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'sign/icon-succes': {
    width: 38,
    height: 28,
    viewBox: '0 0 38 28',
    data: '<path pid="0" d="M36.652 1.348a3.75 3.75 0 010 5.304l-20 20a3.75 3.75 0 01-5.304 0l-10-10a3.75 3.75 0 115.304-5.304L14 18.697 31.348 1.348a3.75 3.75 0 015.304 0z"/>'
  }
})

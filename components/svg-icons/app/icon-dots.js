/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'app/icon-dots': {
    width: 4,
    height: 18,
    viewBox: '0 0 4 18',
    data: '<path pid="0" d="M2 8.75a.25.25 0 100 .5.25.25 0 000-.5zM.25 9a1.75 1.75 0 113.5 0 1.75 1.75 0 01-3.5 0zM2 15.75a.25.25 0 100 .5.25.25 0 000-.5zM.25 16a1.75 1.75 0 113.5 0 1.75 1.75 0 01-3.5 0zM2 1.75a.25.25 0 100 .5.25.25 0 000-.5zM.25 2a1.75 1.75 0 113.5 0 1.75 1.75 0 01-3.5 0z"/>'
  }
})

/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'slider/icon-left': {
    width: 19,
    height: 18,
    viewBox: '0 0 19 18',
    data: '<path pid="0" d="M9.71.293a1 1 0 010 1.414l-8 8A1 1 0 11.294 8.293l8-8a1 1 0 011.414 0z"/><path pid="1" d="M18.002 9a1 1 0 01-1 1h-16a1 1 0 110-2h16a1 1 0 011 1z"/><path pid="2" d="M9.71 17.707a1 1 0 01-1.415 0l-8-8a1 1 0 111.414-1.414l8 8a1 1 0 010 1.414z"/>'
  }
})

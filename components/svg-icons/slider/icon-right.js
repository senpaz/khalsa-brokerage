/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'slider/icon-right': {
    width: 18,
    height: 18,
    viewBox: '0 0 18 18',
    data: '<path pid="0" d="M8.293 17.707a1 1 0 010-1.414l8-8a1 1 0 111.414 1.414l-8 8a1 1 0 01-1.414 0z"/><path pid="1" d="M0 9a1 1 0 011-1h16a1 1 0 110 2H1a1 1 0 01-1-1z"/><path pid="2" d="M8.293.293a1 1 0 011.414 0l8 8a1 1 0 01-1.414 1.414l-8-8a1 1 0 010-1.414z"/>'
  }
})

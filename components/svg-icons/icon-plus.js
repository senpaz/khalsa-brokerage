/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-plus': {
    width: 20,
    height: 20,
    viewBox: '0 0 20 20',
    data: '<path pid="0" d="M1.487 8.948a1.052 1.052 0 000 2.104h7.361v7.36a1.052 1.052 0 002.103 0v-7.36h7.36a1.051 1.051 0 100-2.104h-7.36v-7.36a1.052 1.052 0 10-2.103 0v7.36h-7.36z"/>'
  }
})

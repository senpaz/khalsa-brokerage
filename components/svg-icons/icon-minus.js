/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-minus': {
    width: 16,
    height: 2,
    viewBox: '0 0 16 2',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M.25 1A.75.75 0 011 .25h14a.75.75 0 010 1.5H1A.75.75 0 01.25 1z" _fill="#6D768A"/>'
  }
})

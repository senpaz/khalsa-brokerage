/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-line-arrow': {
    width: 12,
    height: 8,
    viewBox: '0 0 12 8',
    data: '<path pid="0" d="M.18.81A.608.608 0 01.986.747l.069.061L6 5.855 10.946.81a.609.609 0 01.804-.06l.07.06a.64.64 0 01.059.82l-.06.07-5.383 5.492a.608.608 0 01-.803.06l-.07-.06L.182 1.7a.639.639 0 010-.89z"/>'
  }
})

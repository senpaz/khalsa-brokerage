/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'icon-arrow-right': {
    width: 32,
    height: 20,
    viewBox: '0 0 32 20',
    data: '<path pid="0" d="M.5 10A1.5 1.5 0 012 8.5h28a1.5 1.5 0 010 3H2A1.5 1.5 0 01.5 10z"/><path pid="1" d="M31.06 8.94a1.5 1.5 0 010 2.12l-8 8a1.5 1.5 0 01-2.12-2.12l8-8a1.5 1.5 0 012.12 0z"/><path pid="2" d="M20.94.94a1.5 1.5 0 012.12 0l8 8a1.5 1.5 0 01-2.12 2.12l-8-8a1.5 1.5 0 010-2.12z"/>'
  }
})

/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'sidebar/support': {
    width: 18,
    height: 20,
    viewBox: '0 0 18 20',
    data: '<path pid="0" d="M3 10.75c-.69 0-1.25.56-1.25 1.25v2a1.25 1.25 0 102.5 0v-2c0-.69-.56-1.25-1.25-1.25zM.25 12a2.75 2.75 0 115.5 0v2a2.75 2.75 0 11-5.5 0v-2zM15 10.75c-.69 0-1.25.56-1.25 1.25v2a1.25 1.25 0 102.5 0v-2c0-.69-.56-1.25-1.25-1.25zM12.25 12a2.75 2.75 0 115.5 0v2a2.75 2.75 0 11-5.5 0v-2z"/><path pid="1" d="M9 1.75A7.25 7.25 0 001.75 9v3a.75.75 0 01-1.5 0V9a8.75 8.75 0 1117.5 0v3a.75.75 0 01-1.5 0V9A7.25 7.25 0 009 1.75zM15 15.25a.75.75 0 01.75.75c0 1.224-.954 2.183-2.172 2.792-1.253.627-2.9.958-4.578.958a.75.75 0 010-1.5c1.505 0 2.91-.3 3.907-.8 1.033-.516 1.343-1.082 1.343-1.45a.75.75 0 01.75-.75z"/>'
  }
})

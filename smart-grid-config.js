const smartgrid = require('smart-grid')

/* It's principal settings in smart grid project */
const settings = {
  outputStyle: 'scss',
  /* less || scss || sass || styl */
  columns: 12,
  /* number of grid columns */
  offset: '40px',
  /* gutter width px || % || rem */
  mobileFirst: false,
  /* mobileFirst ? 'min-width' : 'max-width' */
  container: {
    maxWidth: '1340px',
    fields: '30px'
    /* max-width оn very large screen */
  },
  breakPoints: {
    lg: {
      width: '1279px', /* -> @media (max-width: 1279px) */
      offset: '30px'
    },
    md: {
      width: '991px'
    },
    sm: {
      width: '767px',
      fields: '15px'
    },
    xs: {
      width: '575px'
    },
    xxs: {
      width: '479px'/* set fields only if you want to change container.fields */
    }
    /*
        We can create any quantity of break points.

        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
  }
}

smartgrid('./assets/scss/vendors', settings)

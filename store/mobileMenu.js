export const state = () => ({
  open: false
})

export const mutations = {
  TOGGLE_MENU (state) {
    state.open = !state.open
  }
}

export const actions = {
  toggleMenu (context) {
    const htmlTag = document.getElementsByTagName('html')
    htmlTag[0].classList.toggle('mobile_menu_open')
    context.commit('TOGGLE_MENU')
  }
}
export const getters = {

}

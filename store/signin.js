export const state = () => ({
  isAuth: false,
  userId: {},
  email: null
})

export const mutations = {
  SET_USER_Id (state, payload) {
    state.userId = payload
  },
  SET_USER_EMAIL (state, payload) {
    state.email = payload
  }

}
export const actions = {
  async createCarrier ({ state }, payload) {
    // eslint-disable-next-line no-useless-catch
    try {
      await this.$api.register.createCarrier({ CarrierCreateForm: { ...payload, user_id: state.userId } })
    } catch (error) {
      throw error
    }
  },
  async createCompany (_, payload) {
    // eslint-disable-next-line no-useless-catch
    try {
      const form = new FormData()
      Object.entries(payload).forEach(([key, value]) => {
        form.append(key, value)
      })
      await this.$api.register.createCompany(form)
    } catch (error) {
      throw error
    }
  }

}
export const getters = {
  getUserId: state => state.userId,
  getUserEmail: state => state.email
}

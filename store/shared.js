export const state = () => ({
  loading: false,
  error: null
})

export const mutations = {
  SET_LOADING (state, payload) {
    state.loading = payload
  },
  SET_ERROR (state, payload) {
    state.error = payload
  },
  CLEAR_ERROR (state) {
    state.error = null
  }
}
export const getters = {
  loading: state => state.loading,
  error: state => state.error
}

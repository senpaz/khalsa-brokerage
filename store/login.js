/* eslint-disable no-useless-catch */
export const state = () => ({
  isLoggedIn: false,
  csrf: null,
  userEmail: null,
  confirmCode: null
})

export const mutations = {
  AUTH_SUCCESS (state) {
    state.isLoggedIn = true
  },
  AUTH_ERROR (state) {
    state.isLoggedIn = false
  },
  SET_CSRF (state, csrf) {
    state.csrf = csrf
  },
  SET_USER_EMAIL (state, payload) {
    state.userEmail = payload
  },
  SET_CONFIRM_CODE (state, payload) {
    state.confirmCode = payload
  }

}
export const actions = {
  async login ({
    commit
  }, payload) {
    try {
      const {
        data
      } = await this.$api.auth.login(payload)

      const {
        access_token: accessToken,
        refresh_token: refreshToken,
        expires_in: expires
      } = data
      this.$cookies.set(process.env.APP_COOKIE_PREFIX + 'access_token', accessToken, {
        maxAge: expires,
        domain: process.env.NODE_ENV === 'development' ? '' : process.env.APP_COOKIE_DOMAIN,
        path: '/',
        secure: true
      })
      this.$cookies.set(process.env.APP_COOKIE_PREFIX + 'refresh_token', refreshToken, {
        maxAge: 60 * 60 * 24 * 365,
        domain: process.env.NODE_ENV === 'development' ? '' : process.env.APP_COOKIE_DOMAIN,
        path: '/',
        secure: true
      })
      commit('AUTH_SUCCESS')
    } catch (e) {
      commit('AUTH_ERROR')
      throw e
    }
  },
  refreshToken ({
    commit, state
  }) {
    if (!state.refreshTokenPromise) {
      const p = new Promise((resolve, reject) => {
        try {
          const res = this.$api.auth.refreshToken({
            refresh_token: this.$cookies.get(process.env.APP_COOKIE_PREFIX + 'refresh_token')
          })
          resolve(res)
        } catch (e) {
          reject(e)
        }
      })
      commit('setRefreshTokenPromise', p)
      p.then((res) => {
        commit('setRefreshTokenPromise', null)
        const { access_token: access, refresh_token: refresh, expires_in: expires } = res
        this.$cookies.set(process.env.APP_COOKIE_PREFIX + 'access_token', access, {
          maxAge: expires,
          domain: process.env.NODE_ENV === 'development' ? '' : process.env.APP_COOKIE_DOMAIN,
          path: '/',
          secure: true
        })

        this.$cookies.set(process.env.APP_COOKIE_PREFIX + 'refresh_token', refresh, {
          maxAge: 60 * 60 * 24 * 365,
          domain: process.env.NODE_ENV === 'development' ? '' : process.env.APP_COOKIE_DOMAIN,
          path: '/',
          secure: true
        })
        commit('AUTH_SUCCESS')
      })
      p.catch((err) => {
        commit('setRefreshTokenPromise', null)
        this.$router.push('/')
        return err
      })
    }
    return state.refreshTokenPromise
  },
  async recoveryPassword ({ commit }, payload) {
    try {
      console.log(payload)
      const { data } = await this.$api.auth.recoveryPassword(payload)
      commit('SET_USER_EMAIL', payload.email)
      return data
    } catch (error) {
      throw error
    }
  },
  async SendCodeAgain ({ state }) {
    try {
      const { data } = await this.$api.auth.resendCode({ email: state.userEmail })
      return data
    } catch (error) {
      throw error
    }
  },
  async setPassword ({ state }, payload) {
    try {
      const { data } = await this.$api.auth.setPassword({ password: payload, confirm_code: state.confirmCode })
      return data
    } catch (error) {
      throw error
    }
  }

}
export const getters = {}

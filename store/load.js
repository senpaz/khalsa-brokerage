export const state = () => ({
  states: [],
  equipments: []
})

export const mutations = {
  SET_STATES (state, payload) {
    state.states = payload
  },
  SET_EQUIPMENTS (state, payload) {
    state.equipments = payload
  }

}

export const actions = {
  async fetchContainerSize () {
    const { data: { data } } = await this.$api.load.fetchContainerSize()

    return data
  },
  async fetchState ({ commit }) {
    const { data: { data } } = await this.$api.load.fetchState()
    commit('SET_STATES', data)
    return data
  },
  async fetchDocTypes () {
    const { data: { data } } = await this.$api.load.fetchDocTypes()
    return data
  },
  async fetchEquipments ({ commit }) {
    const { data: { data } } = await this.$api.load.fetchEquipments()
    commit('SET_EQUIPMENTS', data)
    return data
  },
  async uploadDocuments (_, { form, id }) {
    const { file, docType } = form
    const formData = new FormData()
    formData.append('file', file)
    formData.append('doc_type', docType)

    const { data: { data } } = await this.$api.load.uploadDocuments({ formData, id })
    return data
  }

}

export const getters = {
  getStates: state => state.states,
  getEquipments: state => state.equipments
}

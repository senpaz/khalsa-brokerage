export const state = () => ({
  transparent: true,
  white: false,
  dark: false
})

export const mutations = {
  SET_TRANSPARENT (state, payload) {
    state.transparent = payload
  },
  SET_WHITE (state, payload) {
    state.white = payload
  },
  SET_DARK (state, payload) {
    state.dark = payload
  }

}

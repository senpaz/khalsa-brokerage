import * as dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import 'dayjs/locale/en'
import 'dayjs/locale/ru'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(utc)
dayjs.extend(timezone)
// dayjs.locale('en')
export default function (_, inject) {
  inject('dayjs', dayjs)
}

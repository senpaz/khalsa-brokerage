import Vue from 'vue'
import handleError from '~/mixins/handleError'

Vue.mixin(handleError)

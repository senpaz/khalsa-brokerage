export default function ({ $axios, $auth, $cookies, store, redirect }) {
  // console.log($auth)
  if (process.server) {
    const refreshAccessToken = function (oldRefreshToken) {
      // eslint-disable-next-line no-async-promise-executor
      return new Promise(async (resolve, reject) => {
        try {
          await $auth.refreshTokens()
          $axios.setHeader('X-CSRF-TOKEN', $cookies.get('csrf'))
          resolve()
        } catch (e) {
          reject(e)
        }
      })
    }
    $axios.onRequest((config) => {
      if (!config.headers.common['X-CSRF-TOKEN'] && $cookies.get('csrf') !== null) {
        config.headers.common['X-CSRF-TOKEN'] = $cookies.get('csrf')
      }

      return config
    })
    $axios.onResponseError(async (error) => {
      if (error.response.status === 401 && store.state.auth.isLoggedIn) { // If error is means auth error and user was logged in
        if (error.config.url.includes('/auth/r/token')) { // If error from refresh tokens request
          $cookies.removeAll()
          return redirect('/login')
        } else { // If error from other request
          try { // Try to refresh tokens and repeat a request
            await refreshAccessToken()
            return $axios({
              method: error.config.method,
              url: error.config.url,
              data: error.config.data,
              params: error.config.params
            })
          } catch (err) {
            $cookies.removeAll()
            return redirect('/login')
          }
        }
      } else {
        throw error
      }
    })
  } else {
    $axios.onRequest((config) => {
      if (!config.headers.common['X-CSRF-TOKEN'] && $cookies.get('csrf') !== null) {
        config.headers.common['X-CSRF-TOKEN'] = $cookies.get('csrf')
      }

      return config
    })
    $axios.onResponseError(async (error) => {
      if (!$axios.isCancel(error)) {
        if (error.response.status === 401 && store.state.login.isLoggedIn) { // If error is means auth error and user was logged in
          if (error.config.url.includes('/auth/r/token')) { // If error from refresh tokens request
            $cookies.removeAll()
            return redirect('/login')
          } else {
            await $auth.refreshTokens()
            $axios.setHeader('X-CSRF-TOKEN', $cookies.get('csrf'))
            return $axios({
              method: error.config.method,
              url: error.config.url,
              data: error.config.data,
              params: error.config.params
            })
          }
        } else {
          return Promise.reject(error)
        }
      } else {
        return Promise.reject(error)
      }
    })
  }
}

export default {
  'carrier/dashboard': '/app/carrier/dashboard',
  'carrier/load': id => `/app/carrier/dashboard/${id}`,
  'carrier/search-loads': '/app/carrier/search-loads',
  'carrier/post-truck': '/app/carrier/post-truck',
  'carrier/messages': '/app/carrier/messages',
  'carrier/quote': '/app/carrier/quote',
  'carrier/settings': '/app/carrier/settings',
  'carrier/settingsAccount': '/app/carrier/settings/account',
  'carrier/settingsMyCards': '/app/carrier/settings/my-cards',
  'carrier/settingsChangePassword': '/app/carrier/settings/change-password',
  'carrier/settingsRating': '/app/carrier/settings/rating-reviews',
  'carrier/settingsRates': '/app/carrier/settings/default-rates',
  'carrier/support': '/app/carrier/support'
}

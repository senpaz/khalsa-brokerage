import staticRoutes from './staticRoutes'
import appCarrierRoutes from './appCarrierRoutes'
import appBrokerRoutes from './appBrokerRoutes'

const routes = {
  ...staticRoutes,
  ...appCarrierRoutes,
  ...appBrokerRoutes
}

export function getRoutePath (path, ...additionalArgs) {
  const route = routes[path]

  if (typeof route === 'function') { return route(...additionalArgs) }
  return route
}

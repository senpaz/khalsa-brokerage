export default $axios => ({
  async fetchContainerSize () {
    return await $axios.get('/api/lists/container')
  },
  async fetchState () {
    return await $axios.get('/api/lists/state')
  },
  async fetchDocTypes () {
    return await $axios.get('/api/lists/load-doc-types')
  },
  async fetchEquipments () {
    return await $axios.get('/api/lists/equipment')
  },
  async uploadDocuments ({ formData, id }) {
    return await $axios.post(`/api/load/${id}/document`, formData)
  }

})

import auth from './auth'
import register from './register'
import load from './load'

export default $axios => ({
  auth: auth($axios),
  register: register($axios),
  load: load($axios)
})

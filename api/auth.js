// const api = '/auth'
export default $axios => ({
  async login (payload) {
    try {
      return await $axios.post('/auth/p/token', payload)
    } catch (e) {
      throw new Error(e)
    }
  },
  async  refreshToken (payload) {
    try {
      return await $axios.post('/auth/r/token', payload)
    } catch (e) {
      throw new Error(e)
    }
  },
  async recoveryPassword (payload) {
    return await $axios.post('/auth/recovery', payload)
  },
  async resendCode (payload) {
    return await $axios.post('/auth/recovery/resend', payload)
  },
  async setPassword (payload) {
    return await $axios.post('/auth/recovery/confirm', payload)
  }
})

// const api = '/auth'
export default $axios => ({
  async register (payload) {
    return await $axios.post('/auth/user/create', payload)
  },
  async confirmEmail (payload) {
    try {
      return await $axios.post('/auth/user/confirm-email', payload)
    } catch (e) {
      throw new Error(e)
    }
  },
  async createCarrier (payload) {
    return await $axios.post('auth/carrier', payload)
  },
  async resendCode (payload) {
    return await $axios.post('/auth/resend', payload)
  },
  async createCompany (payload) {
    return await $axios.post('auth/company', payload)
  }

})

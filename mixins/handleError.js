export default {
  methods: {
    handleError (error) {
      const {
        data
      } = error.response
      this.$notify({
        type: 'error',
        title: data.error,
        text: data.error_description
      })
    }
  }
}
